#!/usr/bin/env python
# coding: utf-8

# Part 1

# In[1]:


"""1. Read in a fasta file that contains multiple DNA sequences. These sequences are made up
of upper and lower cases. The upper cases indicate exons, the lower cases indicate
introns. As you might know, only the exons are translated and transcribed to proteins
based on the codon mapping. Each 3 nucleotides map for one amino acid.
Each sequence has its own ID and header starting with “>”. After reading the sequences,
print out the number of sequences in the files."""

import os

# Asking for file through user input + validating it.
# Accepted file is an existing, readable fasta file path: ./.../file.fasta
# Intended file: FinalProject.fasta
cwd = os.getcwd()
print(cwd)

names_path = input("Please input the path to the .fasta file with the DNA data: ")

while not os.path.isfile(names_path) or names_path.find(".fasta") < 0:
    names_path = input("Please input a valid file path to the fasta file: ")
    
    if os.path.isfile(names_path):
        try:
            names_file = open(names_path)
        except:
            print("Please check the file has the right settings")
            names_path = ""

# Initializing data structures to store and organize file content
sequence_ids = []
sequences = {}
mRNAs = {}
introns = {}
exons = {}
upstreams = {}
downstreams = {}

#file = "FinalProject.fasta"
file = names_path
content = open(file)
content = content.readlines()
line_number = -1
sequences_count = -1

# reading file by line
for line in content:
    line_number += 1
    
    # check if line is a sequence ID (first or next sequence)
    # Sequence ID is stored and sequence and mRNA hash indeces (by ID) are initialized
    if line[0:1] == ">":
        sequences_count += 1
        sequence_ids.append(line[1:].strip())
        sequences[sequence_ids[sequences_count]] = ["",]
        mRNAs[sequence_ids[sequences_count]] = ""
        
        # If a previous sequence was already recorded, remove any possible '-' (missing bases)
        if not sequences_count == 0:
            sequences[sequence_ids[sequences_count - 1]][0] = sequences[sequence_ids[sequences_count - 1]][0].replace('-','')
    # If the line is part of a sequence, append it to the current one
    else:
        sequences[sequence_ids[sequences_count]][0] += line.strip()

        # If the line is the last, also remove any possible '-' (missing bases) to process last sequence
        # and print complete count of sequences in file
        if line_number == len(content) - 1: 
            sequences[sequence_ids[sequences_count]][0] = sequences[sequence_ids[sequences_count]][0].replace('-','')
            print("\nYour file has "+str(len(sequence_ids))+" sequences.")
            print("\n-----------------------------------\n")

#for x,y in sequences.items():
#    print("\n>"+x)
#    print(y[0])"""


# In[2]:


"""2. For each sequence compute the number of exons and introns as well as the average
length of the exons and introns. You also need to compute the proportion of each
nucleotide bases, A, C, G, T in the Exon and Intron. """

# Initializing a new output file
outputFile_name = "DNAstats.txt"
outputFile = open(outputFile_name, "a")

# Printing header and writing it to file
print("{:<15} {:<10} {:<10} {:<15} {:<15} {:<13} {:<13} {:<13} {:<13} {:<13} {:<13} {:<13} {:<13}".format('SequenceID','#Exons','#Introns', 'AvgExonLength', 'AvgIntronLeng', '%A in Exon', '%C in Exon', '%G in Exon', '%T in Exon', '%A in Intron', '%C in Intron', '%G in Intron', '%T in Intron'))
#outputFile.write("SequenceID\t#Exons\t#Introns\tAvgExonLength\tAvgIntronLeng\t%A in Exon\t%C in Exon\t%G in Exon\t%T in Exon\t%A in Intron\t%C in Intron\t%G in Intron\t%T in Intron\n")
outputFile.write("{:<15} {:<10} {:<10} {:<15} {:<15} {:<13} {:<13} {:<13} {:<13} {:<13} {:<13} {:<13} {:<13}".format('SequenceID','#Exons','#Introns', 'AvgExonLength', 'AvgIntronLeng', '%A in Exon', '%C in Exon', '%G in Exon', '%T in Exon', '%A in Intron', '%C in Intron', '%G in Intron', '%T in Intron'))
outputFile.write("\n")

# Looping through sequences to compute statistical values
for id,data in sequences.items():
    # To record number of exons and introns separately
    count_exons = -1
    count_introns = -1

    # To record length of introns and exons, each combined separately 
    lenexons_sum = 0
    lenintrons_sum = 0
    
    previous_base = ''
    
    # To record intron base counts
    introns_basecount = {}
    introns_basecount['a'] = 0
    introns_basecount['c'] = 0
    introns_basecount['t'] = 0
    introns_basecount['g'] = 0
    
    # To temporarily record intron base count. First and last lowercase stretches should not be counted
    # with introns, and this hash helps decide which lowercase count to add (middle ones)
    tempintrons_basecount = {}
    tempintrons_basecount['a'] = 0
    tempintrons_basecount['c'] = 0
    tempintrons_basecount['t'] = 0
    tempintrons_basecount['g'] = 0
    
    # To record exon base counts
    exons_basecount = {}
    exons_basecount['A'] = 0
    exons_basecount['C'] = 0
    exons_basecount['T'] = 0
    exons_basecount['G'] = 0
    
    # Initializing hashes with current sequence id
    upstreams[id] = ""
    downstreams[id] = ""
    exons[id] = []
    introns[id] = ["",]
    
    # Looping through every base in current sequence
    for base in data[0]:
        
        # If base is an uppercase letter (exon)
        if base <= 'Z':
            exons_basecount[base] += 1

            # If base is A, G, or C, append it to mRNA
            if(base == 'A' or base == 'G' or base == 'C'):
                mRNAs[id] += base
            # If base is T, append U to mRNA
            else:
                mRNAs[id] += "U"

            # If current base is not the first, check if this is the start of an exon to process
            # previous lowercase stretch
            if not previous_base == '':
                
                # If previous base was lowercase, then this is the start of an exon
                # previous lowercase stretch can be processed, and new exon is created
                if previous_base >= 'a':
                    exons[id].append(base)
                    
                    # if this is not the first exon, previous lowercase stretch is an intron
                    if count_exons >= 0:
                        count_introns += 1
                        lenintrons_sum += len(introns[id][count_introns])
                    
                        introns_basecount['a'] += tempintrons_basecount['a']
                        introns_basecount['c'] += tempintrons_basecount['c']
                        introns_basecount['t'] += tempintrons_basecount['t']
                        introns_basecount['g'] += tempintrons_basecount['g']
                    
                        tempintrons_basecount['a'] = 0
                        tempintrons_basecount['c'] = 0
                        tempintrons_basecount['t'] = 0
                        tempintrons_basecount['g'] = 0
                    # otherwise the stretch is the upstream sequence
                    else:
                        upstreams[id] = introns[id].pop(0) # remove it from introns and add it to upstreams
                # If previous base was uppercase, continue concatenating current exon
                else:
                    exons[id][count_exons + 1] += base
                previous_base = base # update previous base before looping again
            # If this is the first base in the sequence 
            else:
                previous_base = base # make it the first previous base
                exons[id][count_exons + 1] += base # start the first exon
        # If base is a lowercase letter (upstream: first stretch, intron: middle stretches, downstream: last stretch)
        elif base >= 'a':
            tempintrons_basecount[base] += 1
            
            # If this is not the first base in the sequence
            if not previous_base == '':
                
                # If the previous base was an uppercase letter (exon), this is the start of an intron or downstream sequence
                # and previous exon can be processed
                if previous_base <= 'Z':
                    count_exons += 1
                    lenexons_sum += len(exons[id][count_exons])
                    introns[id].append(base)
                
                # If the previous base was a lowercase letter, continue concatenating the current lowercase stretch
                else:
                    introns[id][count_introns + 1] += base
                previous_base = base  # update previous base before looping again
            # If this is the first base in the sequence   
            else:
                previous_base = base # make it the first previous base
                introns[id][count_introns + 1] += base # start the first lowercase stretch
    
    downstreams[id] = introns[id].pop(-1) # remove last lowercase stretch from introns and add it to downstreams
    
    # Compute and update data
    count_exons += 1
    count_introns += 1
    data.append(count_exons)
    data.append(count_introns)
    
    avg_lenexon = lenexons_sum/count_exons
    avg_lenintron = lenintrons_sum/count_introns
    data.append(avg_lenexon)
    data.append(avg_lenintron)
    
    perc_A = (exons_basecount['A']/lenexons_sum)*100
    perc_C = (exons_basecount['C']/lenexons_sum)*100
    perc_T = (exons_basecount['T']/lenexons_sum)*100
    perc_G = (exons_basecount['G']/lenexons_sum)*100
    
    data.append(perc_A)
    data.append(perc_C)
    data.append(perc_G)
    data.append(perc_T)
    
    perc_a = (introns_basecount['a']/lenintrons_sum)*100
    perc_c = (introns_basecount['c']/lenintrons_sum)*100
    perc_t = (introns_basecount['t']/lenintrons_sum)*100
    perc_g = (introns_basecount['g']/lenintrons_sum)*100
    
    data.append(perc_a)
    data.append(perc_c)
    data.append(perc_g)
    data.append(perc_t)
    
    # Print data and write to output file (DNAstats.txt)
    sequence, Exons, Introns, AvgExonLength, AvgIntronLeng, AinExon, CinExon, GinExon, TinExon, AinIntron, CinIntron, GinIntron, TinIntron = data
    print("{:<15} {:<10} {:<10} {:<15} {:<15} {:<13} {:<13} {:<13} {:<13} {:<13} {:<13} {:<13} {:<13}".format(id, Exons, Introns, round(AvgExonLength, 4), round(AvgIntronLeng, 4), round(AinExon, 4), round(CinExon, 4), round(GinExon, 4), round(TinExon, 4), round(AinIntron, 4), round(CinIntron, 4), round(GinIntron, 4), round(TinIntron, 4)))
    #outputFile.write(id+"\t"+str(Exons)+"\t"+str(Introns)+"\t"+str(round(AvgExonLength, 4))+"\t"+str(round(AvgIntronLeng, 4))+"\t"+str(round(AinExon, 4))+"\t"+str(round(CinExon, 4))+"\t"+str(round(GinExon, 4))+"\t"+str(round(TinExon, 4))+"\t"+str(round(AinIntron, 4))+"\t"+str(round(CinIntron, 4))+"\t"+str(round(GinIntron, 4))+"\t"+str(round(TinIntron, 4))+"\n".expandtabs(10))
    outputFile.write("{:<15} {:<10} {:<10} {:<15} {:<15} {:<13} {:<13} {:<13} {:<13} {:<13} {:<13} {:<13} {:<13}".format(id, Exons, Introns, round(AvgExonLength, 4), round(AvgIntronLeng, 4), round(AinExon, 4), round(CinExon, 4), round(GinExon, 4), round(TinExon, 4), round(AinIntron, 4), round(CinIntron, 4), round(GinIntron, 4), round(TinIntron, 4)))
    outputFile.write("\n")
    
outputFile.close() # close output file (DNAstats.txt)


"""3. After writing the file you should prompt to the user that the file was created."""
print("\nA file called DNAstats.txt containing the DNA stats above was created in the current directory.")
print("\n-----------------------------------")

"""4. You then ask the user to enter the ID of a sequence and print out on the terminal all the
statistics for that sequence from the file. You should read it from the file created in step
2 and print it on the terminal. In case the sequence is not there, you prompt the user
that it is not there and ask him/her to enter another one."""

import subprocess

# Print list of sequence IDs for user to choose
for i in sequence_ids:
    print("\n"+i)

print("\nPlease select a valid sequence ID from the list above:\n")
in_choice = input()

# Validate input
while not in_choice in sequence_ids:
    print("\nThe ID is invalid. Please choose an ID from the list:\n")
    in_choice = input()

print("\n")

# Function to print info and compute other required data
def printInfo(sequence_id):
    map = {"UUU":"F", "UUC":"F", "UUA":"L", "UUG":"L",
        "UCU":"S", "UCC":"S", "UCA":"S", "UCG":"S",
        "UAU":"Y", "UAC":"Y", "UAA":"STOP", "UAG":"STOP",
        "UGU":"C", "UGC":"C", "UGA":"STOP", "UGG":"W",
        "CUU":"L", "CUC":"L", "CUA":"L", "CUG":"L",
        "CCU":"P", "CCC":"P", "CCA":"P", "CCG":"P",
        "CAU":"H", "CAC":"H", "CAA":"Q", "CAG":"Q",
        "CGU":"R", "CGC":"R", "CGA":"R", "CGG":"R",
        "AUU":"I", "AUC":"I", "AUA":"I", "AUG":"M",
        "ACU":"T", "ACC":"T", "ACA":"T", "ACG":"T",
        "AAU":"N", "AAC":"N", "AAA":"K", "AAG":"K",
        "AGU":"S", "AGC":"S", "AGA":"R", "AGG":"R",
        "GUU":"V", "GUC":"V", "GUA":"V", "GUG":"V",
        "GCU":"A", "GCC":"A", "GCA":"A", "GCG":"A",
        "GAU":"D", "GAC":"D", "GAA":"E", "GAG":"E",
        "GGU":"G", "GGC":"G", "GGA":"G", "GGG":"G"}
    # Using grep to find respective line in DNAstats.txt (output file)
    grep_cmd = []
    grep_cmd.extend(["grep", sequence_id , "DNAstats.txt"])
    p = subprocess.Popen(grep_cmd, 
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    output = p.communicate()[0] 
    output = output.decode() # decoding bytes to string
    output.strip()
    
    # Printing header
    print("{:<15} {:<10} {:<10} {:<15} {:<15} {:<13} {:<13} {:<13} {:<13} {:<13} {:<13} {:<13} {:<13}".format('SequenceID','#Exons','#Introns', 'AvgExonLength', 'AvgIntronLeng', '%A in Exon', '%C in Exon', '%G in Exon', '%T in Exon', '%A in Intron', '%C in Intron', '%G in Intron', '%T in Intron'))
    print(output)

    """5. For the sequence selected, you need to print out the mRNA for it and the peptide
        protein sequence that corresponds to it. The mRNA is the combination of all the exons
        for that sequence. So you need to combine all the upper case letters in the sequence
        and replace all the Ts with Us. The protein sequence is the translation of the codons
        (3bases in mRNA code for 1 peptide using the codon table)"""

    print("\n-----------------------\n")
    print("The mRNA sequence:\n\n" + mRNAs[sequence_id])
    print("\n-----------------------\n")
    print("The corresponding peptide sequence:\n")

    start = mRNAs[sequence_id].find("AUG") # Finding M start codon
    aa_count = 0 # initializing amino acid count

    # If a start codon was found
    if start != -1:

        # While a codon still exists
        while start + 2 < len(mRNAs[sequence_id]):
            codon = mRNAs[sequence_id][start:start+3]
            
            # If a stop codon is encountered, stop looping
            if codon == "UAA" or codon == "UGA" or codon == "UAG":
                #start = mRNAs[sequence_id][start+3:].find("AUG")+start+3
                break
            # Otherwise, print corresponding amino acid
            else:
                print(map[codon], end="")
                start = start + 3 # Update start index to read next codon
                aa_count += 1 # update amino acid count

    print("\n\nAA count: " + str(aa_count))

    """6. You need also to print out the RANGE and AVERAGE of melting temperature of the
        primers for that sequence. For this you need to consider the lower case letter before the
        first exon (highlighted in yellow). You need then to consider a window of 20 bases, and
        for each window 20, you compute the melting temperature using the following formula
        Tm = ( A + T ) * 2°C + ( C + G ) * 4°C
        So once you compute the Tm for each window of size 20, you print out the Range, minmax,
        and the average."""

    start = 0 # Starting at beginning of upstream sequence (first base) with first frame
    # Initializing base count hash
    upstream_count = {}
    upstream_count['a'] = 0
    upstream_count['t'] = 0
    upstream_count['c'] = 0
    upstream_count['g'] = 0

    melting_temps = [] # Meling temperatures list
    melting_temps_sum = 0 # melting temperatures sum

    # While a frame of 20 bases still exists
    while start + 19 < len(upstreams[sequence_id]):
        frame = upstreams[sequence_id][start:start+20]

        # if this is the first frame, compute and register base count
        if start == 0:
            for base in frame:
                upstream_count[base] += 1
        # otherwise, remove base at start - 1 from base count and add last base in current frame to base count
        else:
            upstream_count[upstreams[sequence_id][start - 1]] -= 1
            upstream_count[frame[-1]] += 1
            
        start += 1 # update start index (next frame)
        # Compute melting temperature and record it
        mtemp = (upstream_count['a'] + upstream_count['t']) * 2 + (upstream_count['g'] + upstream_count['c']) * 4
        melting_temps.append(mtemp)
        melting_temps_sum += mtemp #update Tm sum
    
    print("\n-----------------------\n")
    melting_temps_avg = melting_temps_sum / len(melting_temps) # Compute average
    melting_temps_range = max(melting_temps) - min(melting_temps) # Compute range
    
    print("Average Tm: " + str(round(melting_temps_avg, 4)) + "°C")
    print("\nTm Range: " + str(melting_temps_range)  + "°C")
    print("\nNumber of frames: " + str(start))

printInfo(in_choice) # Call function with user input choice

# Part 2

""" Write a reverse transcription function that takes as argument a protein sequence and
generates all the possible mRNA sequences that can lead to this protein.
The function should also compute for each mRNA the %CG content ( how many C and G
are in the sequence compared to the total length).
You should then print out all the mRNA with their %GC content from lowest %GC
content to highest.
Choose the most probable mRNA sequence to be the one with the %GC content closer
to 50 and print it out as well. """

import operator

# Asking user for peptide sequence input
print("\n-----------------------------------\n")
print("Please input a protein sequence:\n")
peptide = input()
print("\n-----------------------\n")
print("Possible mRNAs:\n")

# Function that takes as input a somewhat short peptide sequence and outputs all possible mRNA sequences
# with %GC (GC content) and outputs all most probable mRNA sequences with equal %GCs which represent
# the closest value to 50% among all mRNA possibilities.
# Note that only one stop codon variant of most probable mRNAs (%GC closest to 50) is outputed
# Those possibilities can all also have UAG as a stop codon (having 1 G contributing to the %GC just like UGA)
# Nevertheless, all stop codon variants are included in the full list output of possible mRNAs.
def reverseTranscribe(peptide_seq):
    # A customized map with amino acid as key and list of all possible codons with their respective G+C count
    map = {"F":[["UUU",0],["UUC",1]], "L":[["UUA",0],["UUG",1],["CUC",2],["CUU",1],["CUA",1],["CUG",2]],
        "S":[["UCU",1],["UCC",2],["UCA",1],["UCG",2],["AGC",2],["AGU",1]], "Y":[["UAU",0],["UAC",1]],
        "#":[["UAA",0],["UAG",1],["UGA",1]], "C":[["UGU",1],["UGC",2]], "W":[["UGG",2]],
        "P":[["CCU",2],["CCC",3],["CCA",2],["CCG",3]], "H":[["CAU",1],["CAC",2]],
        "Q":[["CAA",1],["CAG",2]], "R":[["CGU",2],["CGC",3],["CGA",2],["CGG",3],["AGA",1],["AGG",2]],
        "I":[["AUU",0],["AUC",1],["AUA",0]], "M":[["AUG",1]], "T":[["ACU",1],["ACC",2],["ACA",1],["ACG",2]],
        "N":[["AAU",0],["AAC",1]], "K":[["AAA",0],["AAG",1]], "V":[["GUU",1],["GUC",1],["GUA",1],["GUG",2]],
        "A":[["GCU",2],["GCC",3],["GCA",2],["GCG",3]], "D":[["GAU",1],["GAC",2]], 
        "E":[["GAA",1],["GAG",2]], "G":[["GGU",2],["GGC",3],["GGA",2],["GGG",3]]}
    possible_mRNAs = [["",0],]
    peptide_seq += "#" # Add an end sign to process stop codon variants from map
    optimal_mRNAs = [["",0],]
    
    # For every amino acid in the peptide sequence inputted
    for aa in peptide_seq:
        temp_mRNAs = [] # Temp list to copy all results from previous loop and continuously process possible codons except the last
        # which will be automatically added to the main list. the temporary sequence are then copied back to the main list
        # this ensures that the previous result is preserved until the last codon possibility, which generates the previous result of the next amino acid
        
        # For every codon possible for the current amino acid
        for codon in map[aa]:
            
            # If we are at the last possible codon for the amino acid
            if map[aa][len(map[aa]) - 1][0] == codon[0]:
                
                # Add the codon to every mRNA sequence in the main list
                for rna_seq in possible_mRNAs:
                    rna_seq[0] += codon[0]
                    rna_seq[1] += codon[1]
                    
                    # If the end of the peptide sequence is reached
                    if aa == "#":
                        rna_seq[1] = (rna_seq[1] / (len(peptide_seq) * 3)) * 100 # Compute %GC from G+C cout
                         
                        # If no most probable mRNA is recorded yet or if a better one is found, update the most probable mRNAs
                        # If multiple with equal %GC were already found, disregard all and record the new best one
                        if optimal_mRNAs[0][0] == "" or abs(optimal_mRNAs[0][1] - 50) > abs(rna_seq[1] - 50):
                            optimal_mRNAs = [["",0],]
                            optimal_mRNA = ["",0]
                            optimal_mRNA[0] = rna_seq[0]
                            optimal_mRNA[1] += rna_seq[1]
                            optimal_mRNAs[0] = optimal_mRNA
                        
                        # If another most probable mRNA with equal %GC is found, append it to most probable mRNAs list
                        elif abs(optimal_mRNAs[0][1] - 50) == abs(rna_seq[1] - 50):
                            optimal_mRNA = ["",0]
                            optimal_mRNA[0] = rna_seq[0]
                            optimal_mRNA[1] += rna_seq[1]
                            optimal_mRNAs.append(optimal_mRNA)
                            
                        #print(rna_seq[0] + "\n\n" + "%GC: " + str(rna_seq[1]))
                        #print("\n-----------------------\n")
            # If we are at any codon but the last possible one for the current amino acid           
            else:
                
                # Copy every possible mRNA from previous result to temporary list, and append codon to each
                for rna_seq in possible_mRNAs:
                    temp_mRNA_seq = rna_seq[0] + codon[0]
                    temp_mRNA_GC = rna_seq[1] + codon[1]
                    
                    # If end of peptide sequence is reached
                    if aa == "#":
                        temp_mRNA_GC = (temp_mRNA_GC / (len(peptide_seq) * 3)) * 100 # compute %GC
                        
                        # If no most probable mRNA is recorded yet or if a better one is found, update the most probable mRNAs
                        # If multiple with equal %GC were already found, disregard all and record the new best one
                        if optimal_mRNAs[0][0] == "" or abs(optimal_mRNAs[0][1] - 50) > abs(rna_seq[1] - 50):
                            optimal_mRNAs = [["",0],]
                            optimal_mRNA = ["",0]
                            optimal_mRNA[0] = rna_seq[0]
                            optimal_mRNA[1] += rna_seq[1]
                            optimal_mRNAs[0] = optimal_mRNA
                        
                        # If another most probable mRNA with equal %GC is found, append it to most probable mRNAs list   
                        elif abs(optimal_mRNAs[0][1] - 50) == abs(rna_seq[1] - 50):
                            optimal_mRNA = ["",0]
                            optimal_mRNA[0] = rna_seq[0]
                            optimal_mRNA[1] += rna_seq[1]
                            optimal_mRNAs.append(optimal_mRNA)
                            
                        #print(temp_mRNA_seq + "\n\n" + "%GC: " + str(temp_mRNA_GC))
                        #print("\n-----------------------\n")
                        
                    temp_mRNA = [temp_mRNA_seq, temp_mRNA_GC]
                    temp_mRNAs.append(temp_mRNA)
        # Copy all temporary mRNA sequences to main list            
        for temp_mRNA in temp_mRNAs:
            possible_mRNAs.append(temp_mRNA)
    
    # Sort list of lists according to %GC at index 1 of every element (increasing order)
    possible_mRNAs = sorted(possible_mRNAs, key=operator.itemgetter(1))

    # Print sorted possible mRNAs
    for seq in possible_mRNAs:
        print(seq[0] + "\n\n" + "%GC: " + str(seq[1]))  
        print("\n-----------------------\n")   

    print("Total number of possible mRNAs: " + str(len(possible_mRNAs)))
    print("\n-----------------------\n")
    print("Most probable mRNAs:\n")

    # Print optimal mRNAs
    for optimal_mRNA in optimal_mRNAs:
        print(optimal_mRNA[0] + "\n\n")

    print("%GC: " + str(optimal_mRNA[1]))
    print("\n-----------------------\n")
    print("Total number of most probable mRNAs: " + str(len(optimal_mRNAs)))
    print("\n-----------------------------------\n")
    #print(possible_mRNAs)
    
reverseTranscribe(peptide)

