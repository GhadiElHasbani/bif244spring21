#!/usr/bin/perl

print "002\n";
$num = 3;
@primes = (2,);
# Wilson Theorem
BIGLOOP:
while( $num < 1000 )
{
	$length = @primes;
	for ( $i = 1; $i < $length && @primes[$i] < sqrt($num); $i += 1)
	{
		if( $num%(@primes[$i]) == 0 )
		{
			$num += 2;
			next(BIGLOOP);
		}
	}
	printf "%03d\n", $num;
	push @primes, $num;
	$num += 2;
}

# real	0m0.019s
# user	0m0.011s
# sys	0m0.006s
# for prime numbers up to 1000 (sys range is 0m0.004-0m0.006)

# Dice with for loop

$sum = 0;

for ($i = 1; $i < 7; $i += 1)
{
	$dice = int rand(5) + 1;
	print "\nDice number $i has the value $dice.\n";
	$sum += $dice;
}

print "\nThe sum of all six dice is $sum.\n";

# Dice with while loop

$sum = 0;
$count = 1;

while ( $count < 7)
{
	$dice = int rand(5) + 1;
	print "\nDice number $count has the value $dice.\n";
	$sum += $dice;
	$count += 1;
}

print "\nThe sum of all six dice is $sum.\n";

# Numbers from input unless input = 13
$user = 1;
$sum = 0;

while ( $user )
{
	print "\nInput a number to add to the sum and 13 to stop. 9 and 18 will be skipped.\n";
	$user_num = <STDIN>;

	if($user_num == 9 || $user_num == 18)
	{
		next;
	} elsif($user_num == 13)
	{
		$user = 0;
	} else
	{
		$sum += $user_num;
	}
}

print "The sum of your numbers, excluding 9 and 18, is $sum.\n";

# Comparing strings

print "\nPlease input a long string: \n";
$long_str = <STDIN>;
chomp $long_str;

print "\nPlease input a shorter string to find intersection of both: \n";
$short_str = <STDIN>;
chomp $short_str;

$ind = index $long_str, $short_str;
print "Using index: $ind.\n";

$rind = rindex $long_str, $short_str;
print "Using rindex: $rind.\n";

if($ind == $rind)
{
	print "Same result.\n";
} else
{
	print "Different result.\n";
}