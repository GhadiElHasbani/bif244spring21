#!/usr/bin/perl

@array = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
@even = ();
@prime = ();
print "\nNumbers from 1-10 are: \n";

foreach $i (@array)
{
	if($i%2 == 0)
	{
		push @even, $i;
		if ( $i == 2)
		{
			push @prime, $i;
		}
	} else
	{
		if( $i != 1 && $i != 9 )
		{
			push @prime, $i;
		}
	}
	print "$i\n";
}

print "\nEven numbers from 1-10 are: \n";
foreach $i (@even)
{
	print "$i\n";
}

print "\nPrime numbers from 1-10 are: \n";
foreach $i (@prime)
{
	print "$i\n";
}

$len_all = @array;
$len_even = @even;
$len_prime = @prime;

print "\nLength of numbers array: $len_all";
print "\nLength of even numbers subarray: $len_even";
print "\nLength of prime numbers subarray: $len_prime\n";

# Sorting arrays

@names = ("Ghadi", "Jovi", "Sarah", "Thea", "Lynn", "Tina");
@sorted_names = sort(@names);

print "\nList of names: \n";
foreach $i (@names)
{
	print "$i\t";
}

print "\nList of sorted names according to ASCII: \n";
foreach $i (@sorted_names)
{
	print "$i\t";
}

print "\n";

# Spaceship Operators

@randos = ();
for ($i = 0; $i < 25; $i += 1 )
{
	$rando = int rand(101);
	push @randos, $rando;
}

@sorted_randos = sort { $a <=> $b } @randos;

print "\nList of random numbers: \n";
foreach $i (@randos)
{
	print "$i\t";
}

print "\nList of sorted random numbers using a spaceship operator: \n";
foreach $i (@sorted_randos)
{
	print "$i\t";
}

print "\n";

# Split function

@albums = (
	"Full Circle,1972,The Doors", 
	"The Dark Side of the Moon,1973,Pink Floyd", 
	"Sgt. Pepper's Lonely Hearts Club Band,1967,The Beatles", 
	"Beggars Banquet,1968,The Rolling Stones", 
	"Flowers,1967,The Rolling Stones"
	);

print "\nArtist\tAlbum Title";
foreach $i (@albums)
{
	@line = split( /,/, $i );
	print "\n@line[2]\t@line[0]";
}

print "\n";