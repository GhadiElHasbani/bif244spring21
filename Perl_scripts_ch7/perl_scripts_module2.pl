#!/usr/bin/perl

$dice1 = int rand(5) + 1;
$dice2 = int rand(5) + 1;
$dice3 = int rand(5) + 1;
$dice4 = int rand(5) + 1;
$dice5 = int rand(5) + 1;
$dice6 = int rand(5) + 1;

print "Please input a guess: \n";
$guess_value = <STDIN>;
chomp $guess_value;

$sum = $dice1 + $dice2 + $dice3 + $dice4 + $dice5 + $dice6;
print "Your sum is: $sum \n";

if ( $sum > 20 )
{
	print "You win! :)\n";
}
else
{
	print "You lose! :(\n";
}

if ( $sum > $guess_value )
{
	print "Sum is greater than $guess_value \n";
}
else
{
	if ( $sum == $guess_value )
	{
		print "Sum is equal to $guess_value \n";
	}
	else
	{
		print "Sum is smaller than $guess_value \n";
	}
}

if ( $sum%2 == 0 )
{
	print "Sum is even \n";
}
else
{
	print "Sum is odd \n";
}

# Strings
$animal1 = "cat";
$animal2 = "dog";
$animal3 = "rat";

print "The available animals are $animal1, $animal2, and $animal3.\n";
$rand_choice = rand(2) + 1;
$choice = "";
if( $rand_choice == 1)
{
	$choice = $animal1;
}
elsif( $rand_choice == 2)
{
	$choice = $animal2;
}
else
{
	$choice = $animal3;
}

print "Guess the animal chosen: \n";
$guess = <STDIN>;
chomp $guess;

if( $guess eq $choice)
{
	print "You guessed correctly! :)\n";
}
else
{
	print "You guessed incorrectly! :(\n";
}

# Temperature
print "Input the outside temperature in degrees Fahrenheit: \n";
$ftemp = <STDIN>;
chomp $ftemp;

$ctemp = ($ftemp - 32)*5/9;
print "Your outside temperate in degrees Celsius is $ctemp.\n";