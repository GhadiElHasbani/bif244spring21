#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Exercise: 
# read fasta file into a list

# use FPexcerpt.fta as the input. (Attached here)

# Outline:
# 1. Get name of file from user input (extra credit, make it do multiple files).
# To start: get it to print the file name.

# 2. Use sys.stderr.write() to give feedback
#   (Won't be captured by >)
   
# 3. Print help if the user doesn't enter a file name

# 4. open file, read in lines:
#  (extra credit: skip blank, header, and comment lines)

# 5. if it is a sequence name, save it in a list, increment the sequence number. 
# set up a blank sequence to go along with it

# 6. if it is a portion of a sequence, strip out [spaces, \n, dashes] and append to existing sequence

# 7. when done, use a new for loop to go through and print out a table of size, pct composition AGCT

import sys
import os

print("Hello! Please enter the name(s) of the file(s) you'd like to read! Type 'done' when done entering files")

path = os.getcwd()
print("Your current working directory:", path)
files_in_dir = os.listdir(path)
print("Available files:", files_in_dir)
sequence_names = []
sequences = []
count = -1
file = input()

while not file == "done":

    if file in files_in_dir:
        print("The file being analyzed is " + file + ". Type 'done' when done entering files")
        content = open(file)
        content = content.readlines()
    
        for x in content:
            if x[0:1] == ">":
                if not count == -1:
                    sequences[count] = sequences[count].replace('-', '')
                x = x.strip()
                sequence_names.append(x[1:])
                sequences.append("")
                count = count + 1
            elif not(x == "" and x[0:1] == "#"):
                sequences[count] = sequences[count] + x.strip() # concat
    elif file == "":
        sys.stderr.write("This exercise is to get the percent composition of sequences. Please enter a file.")
    else: # if file not in directory
        sys.stderr.write("File not found. Please enter a valid file name.")
    file = input()

all_hash = {}
for i in range(len(sequences)):
    seq = sequences[i]
    countA = (seq.count("A")/len(seq))*100
    countG = (seq.count("G")/len(seq))*100
    countC = (seq.count("C")/len(seq))*100
    countT = (seq.count("T")/len(seq))*100
    name = sequence_names[i]
    all_hash[name] = [len(seq), round(countA, 2), round(countG, 2), round(countC, 2), round(countT, 2)]
    
print("{:<15} {:<10} {:<10} {:<10} {:<10} {:<10}".format('Name','Length','%A','%G','%C','%T'))
for key, value in all_hash.items():
    length, percA, percG, percC, percT = value
    print("{:<15} {:<10} {:<10} {:<10} {:<10} {:<10}".format(key, length, percA, percG, percC, percT))


# In[1]:


#Write a Python program that takes length of 3 sides forming a triangle and checks if it is a right triangle

print("What are the lengths of the triangle sides?")
lengths = input()
nums = lengths.split()
x,y,z = sorted(nums)
x = int(x)
y = int(y)
z = int(z)

if x**2 + y**2 == z**2:
    print("It's a right triangle!")
else:
    print("Not a right triangle.")


# In[2]:


# Write a Python program to validate a password input
# Rules:
# 1. Length between 6 and 12 characters inclusive
# 2. Contains at least 1 lowercase letter [a-z]
# 3. Contains at least 1 uppercase letter [A-Z]
# 4. Contains at least 1 digit [0-9]
# 5. Contains at least 1 special character [$#@]
# 6. Does not contain a space '\s'

import re
p = input("Input your password")
x = True
while x:
    if len(p) < 6 or len(p) > 12:
        break
    elif not re.search("[a-z]", p):
        break
    elif not re.search("[A-Z]", p):
        break
    elif not re.search("[0-9]", p):
        break
    elif not re.search("[$#@]", p):
        break
    elif re.search("\s", p):
        break
    else:
        print("Valid password.")
        x = False
        break

if x:
    print("Not a valid password.")


# In[ ]:




