#!/usr/bin/env python
# coding: utf-8

#Create a dictionary that saves the molecualr weight of all proteins in a fasta file
#Read the fasta file and define your dictionary as follows
#The key should be the protein ID
#the values should be the Molecular weight and the sequence
# make sure the sequence does not have the "-" in it.

# In[1]:


AminoDict={
'A':89.09,
'R':174.20,
'N':132.12,
'D':133.10,
'C':121.15,
'Q':146.15,
'E':147.13,
'G':75.07,
'H':155.16,
'I':131.17,
'L':131.17,
'K':146.19,
'M':149.21,
'F':165.19,
'P':115.13,
'S':105.09,
'T':119.12,
'W':204.23,
'Y':181.19,
'V':117.15,
'X':0.0,
'-':0.0,
'*':0.0 }


# In[2]:


import os

path = os.getcwd()

print("Hello! Please enter the name of the file you'd like to read")
print("Your current working directory:", path)
files_in_dir = os.listdir(path)
print("Available files:", files_in_dir)
file = input()

if file in files_in_dir:
        print("The file being analyzed is " + file)
        content = open(file)


# In[3]:


Seqs = {}
MWs = {}
keys = []
count = -1

for line in content.readlines():
    line = line.strip()
    if line[0:1] == ">":
        count += 1
        current_seq = line[1:]
        Seqs[current_seq] = ""
        MWs[current_seq] = ["",]
        keys.append(current_seq)
    else:
        Seqs[keys[count]] += line.replace('-', '')
        MWs[keys[count]][0] += line.replace('-', '')

for seq in Seqs:
    MW = 0
    for aa in Seqs[seq]:
        MW += AminoDict[aa]

    seq_vals = MWs[seq]
    seq_vals.append(MW)

print("Sequences: ")
print("")
for key, val in Seqs.items():
    print(key + ":")
    print(val)
    print("")

print("")
print("")

print("{:<15} {:<10}".format('Sequence ID', 'MW'))
for key, vals in MWs.items():
    print("{:<15} {:<10}".format(key, vals[1]))


# In[ ]:




