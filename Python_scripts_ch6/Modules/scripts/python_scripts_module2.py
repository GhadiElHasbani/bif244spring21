#!/usr/bin/env python
# coding: utf-8

# In[1]:


# if statements require an indented block
# We use the ':' to introduce the block and indent for the block
x = 5

if x < 10:
    print("Hellow")
else:
    print("Bye")


# In[3]:


# Nested if statements require several blocks
if x < 10:
    if x < 5:
        print("Less than 5")
    else:
        print("Greater than or equal to 5, but less than 10")
else:
    print("Greater than 10")


# In[4]:


sum = 50

if sum%2==0:
    print("Even")
else:
    print("Odd")

sum = 75

if sum%2==0:
    print("Even")
else:
    print("Odd")


# In[5]:


# mutltiple if statements can be used by replacing the subsequent conditions with elif
if sum < 40:
    print("Less than 40")
elif sum < 80:
    print("Between 40 and 80")
else:
    print("Greater than or equal to 80")


# In[6]:


# Getting input from the user requires the use of input("Question")
var = input("What is the value of var? ")
print(var)


# In[7]:


# rstrip() replaces chomp in Python
# This function returns the new string
test = "Hellow. My name is XOXO. This is my story\n"
print(test)

print("---------------------")
newString = test.rstrip()
print(newString)

print("---------------------")


# In[9]:


import random

randomNum = random.randrange(100)

guessedNumber = input("What is the number that was generated? ")
guessedNumber = int(guessedNumber)

if randomNum == guessedNumber:
    print("Correctly guessed!")
else:
    print("The number generated was ", randomNum)
    print("The difference is ", randomNum - guessedNumber)


# In[11]:


# Python compares strings using the regular comparison operators
# According to ASCII code
if "A" > "z":
    print("A is greater than z")
else:
    print("It is not")
    
print("A" > "z", "A" == "z", "A" < "z")


# In[12]:


# Logical and (&&) is the 'and' operator
# Logical or (||) is the 'or' operator
# Notice that True and False must be capitalized
# '!' is replaced with the (not)
print(True and False)
print(True or False)
print(not True)


# In[13]:


# Several Conditions may be added and combined
x = 7

if x > 5 and x < 10:
    print("Between 5 and 10")
    
# Parathesis are not required
if (x > 5) and (x < 10):
    print("Between 5 and 10")


# In[14]:


# You can assign parameters based on certain conditions in a shorthand way
a = int(input("What is a? "))
b = int(input("What is b? "))

x = "Greater" if a > b else "Less"

print(x)


# In[ ]:




