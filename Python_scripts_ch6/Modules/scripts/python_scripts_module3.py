#!/usr/bin/env python
# coding: utf-8

# In[5]:


# Looping can be done via a for loop and a while loop
# However the for loop is an iteratory, unlike the classical for loop in other languages
# Its syntax is for [tempVariable] in Object
# You can also loop through a string
for char in "Hellowz":
    print(char)


# In[6]:


# Looping in a range requires the use of range function
# It loops from 0 till the parameter passed excluded
for x in range(20):
    print(x)


# In[8]:


import random
result = 0
sum = 0
min = 1
max = 6

for x in range(6):
    result = random.randrange(1, 7)
    sum = sum + result
    
    if x == 0:
        min = result
        max = result
    elif min > result:
        min = result
    elif max < result:
        max = result
        
    print("The die's value is ", result)
    
print("The sum is ", sum)
print("The min is ", min)
print("The max is ", max)


# In[22]:


# While maintains the classical syntax
# break works just like any language (In Perl it is last)
# Continue replaces the next statement in Perl
x = 0

while True:
    x = x + 1
    print("x is ", x)
    if x == 2:
        x = x + 1
        print("Skipping an extra step")
        continue
    
    if x == 5:
        print(x)
        break


# In[23]:


# Python does not support labels like Perl
# Labeling while and if blocks in Perl can add specificity to 'break'


# In[24]:


# You can stop a python script via the exit function
exit()


# In[2]:


# The in operator can check for subtrings
if "in" in "Maxinger":
    print("Found")
else:
    print("Not found")
    
# .index() and .find() can be called to find the index of a character
# The main difference is that index returns -1 if not found and raises an error
# find simply returns -1

print("Hellowz".index("ll"))
print("Hellowz".find("sad"))
print("Hellowz".find("ll"))

# rindex is the same as index except if starts searchign the string from the right side
# helps save computation time if pattern is known to be at the end of the data
print("Hellowz".rindex("ll"))


# In[3]:


# Printing formatted text can be done via the % operator which replaces the variable
# Multiple variables are passed via a tuple (var1, var2) in order. These will be discussed later
print("Hello, my name is %s. I am %d years old." % ("Ghadi", 19))


# In[ ]:




